<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('welcome');
});


// Route::get('/home', 'HomeController@index')->name('home');

// Images
Route::get('/images/{slug}', 'ImagesController@index')->name('images');
Route::get('/images/{slug}/{id}', 'ImagesController@show')->name('view');

// Png
Route::get('/png/{slug}', 'PngController@index')->name('png');
Route::get('/png/{slug}/{id}', 'PngController@show')->name('pngsingle');


// Products
// Digital Painting's
Route::get('/products/digital-painting', 'DigitalPaintingController@index')->name('painting');
Route::post('/products/digital-painting', 'DigitalPaintingController@store')->name('paintingStore');

// Digital Art
Route::get('/products/digital-art', 'DigitalArtController@index')->name('art');
Route::post('/products/digital-art', 'DigitalArtController@store')->name('artStore');

// Pencil Sketch
Route::get('/products/pencil-sketch', 'PencilSketchController@index')->name('sketch');
Route::post('/products/pencil-sketch', 'PencilSketchController@store')->name('sketchStore');

// Caricature
Route::get('/products/caricature', 'CaricatureController@index')->name('caricature');
Route::post('/products/caricature', 'CaricatureController@store')->name('caricatureStore');

// Restoration
Route::get('/products/restoration', 'RestorationController@index')->name('restoration');
Route::post('/products/restoration', 'RestorationController@store')->name('restorationStore');

// Banner's
Route::get('/products/banner', 'BannerController@index')->name('banner');
Route::post('/products/banner', 'BannerController@store')->name('bannerStore');

// Save the Date
Route::get('/save-the-date', 'StdController@index')->name('std');
Route::get('/save-the-date/{id}', 'StdController@show')->name('stdShow');
Route::post('/save-the-date', 'StdController@store')->name('stdStore');

// Search Route's
Route::get('/filter', 'SearchController@filter');
Route::get('/filterpng', 'SearchController@filterpng');
Route::post('/search', 'SearchController@search')->name('search');



// Buy
// Image's
Route::post('/success', 'ImagesController@success')->name('success');
// Png's
Route::post('/png-success', 'PngController@success')->name('pngsuccess');
// Std
Route::post('/save-the-date-success', 'StdController@success')->name('stdsuccess');


// Packages
// Weakly
Route::post('/weekly-success', 'PackagesController@weekly')->name('weekly');
// Monthly
Route::post('/monthly-success', 'PackagesController@monthly')->name('monthly');
// Yearly
Route::post('/yearly-success', 'PackagesController@yearly')->name('yearly');


// Buy Ajax
// Image's
Route::post('/ajaxbuyimage', 'PaymentController@image');
// Png's
Route::post('/ajaxbuypng', 'PaymentController@png');

// Payumoney Endpoint's
Route::get('/payumoney/{id}', 'PayumoneyController@paymentendpoint')->name('payumoney');
Route::get('/payment/status', 'PayumoneyController@status');



// Admin Routes
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

// Testing's
// Route::get('/payu', 'PaymentController@test');