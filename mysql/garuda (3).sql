-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 21, 2019 at 09:18 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `garuda`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2019-12-04 23:05:15', '2019-12-04 23:05:15'),
(2, NULL, 1, 'Category 2', 'category-2', '2019-12-04 23:05:15', '2019-12-04 23:05:15');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
CREATE TABLE IF NOT EXISTS `data_rows` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(81, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(82, 11, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(83, 11, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true}}', 3),
(84, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(85, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(86, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(87, 12, 'cat_id', 'text', 'Cat Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(88, 12, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 6),
(89, 12, 'desc', 'text_area', 'Desc', 0, 1, 1, 1, 1, 1, '{}', 7),
(90, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(91, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(92, 12, 'image_belongsto_image_category_relationship', 'relationship', 'image_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\ImageCategory\",\"table\":\"image_categories\",\"type\":\"belongsTo\",\"column\":\"cat_id\",\"key\":\"slug\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(93, 12, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 3),
(94, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(95, 13, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(96, 13, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true}}', 3),
(97, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(98, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(99, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(100, 14, 'cat_id', 'text', 'Cat Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(101, 14, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 4),
(102, 14, 'desc', 'text', 'Desc', 0, 1, 1, 1, 1, 1, '{}', 5),
(103, 14, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 7),
(104, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(105, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(106, 14, 'png_belongsto_png_category_relationship', 'relationship', 'png_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\PngCategory\",\"table\":\"png_categories\",\"type\":\"belongsTo\",\"column\":\"cat_id\",\"key\":\"slug\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(107, 12, 'thumb', 'image', 'Thumb', 1, 1, 1, 1, 1, 1, '{}', 2),
(108, 14, 'edited', 'select_dropdown', 'Edited', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}', 8),
(109, 14, 'thumb', 'image', 'Thumb', 1, 1, 1, 1, 1, 1, '{}', 6),
(110, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(111, 15, 'mobile', 'number', 'Mobile', 1, 1, 1, 1, 1, 1, '{}', 2),
(112, 15, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 3),
(113, 15, 'faces', 'text', 'Faces', 1, 1, 1, 1, 1, 1, '{}', 4),
(114, 15, 'paper', 'text', 'Paper', 1, 1, 1, 1, 1, 1, '{}', 5),
(115, 15, 'duration', 'text', 'Duration', 1, 1, 1, 1, 1, 1, '{}', 6),
(116, 15, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 7),
(117, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(118, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
CREATE TABLE IF NOT EXISTS `data_types` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2019-12-04 23:05:07', '2019-12-04 23:05:07'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2019-12-04 23:05:07', '2019-12-04 23:05:07'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-12-04 23:05:07', '2019-12-04 23:05:07'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2019-12-04 23:05:14', '2019-12-04 23:05:14'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2019-12-04 23:05:15', '2019-12-04 23:05:15'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2019-12-04 23:05:16', '2019-12-04 23:05:16'),
(11, 'image_categories', 'image-categories', 'Image Category', 'Image Categories', NULL, 'App\\ImageCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-05 00:22:49', '2019-12-05 00:24:37'),
(12, 'images', 'images', 'Image', 'Images', NULL, 'App\\Image', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-05 00:23:31', '2019-12-17 11:42:58'),
(13, 'png_categories', 'png-categories', 'Png Category', 'Png Categories', NULL, 'App\\PngCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-12-13 08:21:00', '2019-12-13 08:21:00'),
(14, 'pngs', 'pngs', 'Png', 'Pngs', NULL, 'App\\Png', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-13 09:14:43', '2019-12-19 04:47:27'),
(15, 'digitalpaintings', 'digitalpaintings', 'Digitalpainting', 'Digitalpaintings', NULL, 'App\\Digitalpainting', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-12-19 08:33:24', '2019-12-19 09:35:19');

-- --------------------------------------------------------

--
-- Table structure for table `digitalpaintings`
--

DROP TABLE IF EXISTS `digitalpaintings`;
CREATE TABLE IF NOT EXISTS `digitalpaintings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `faces` int(11) NOT NULL,
  `paper` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `digitalpaintings`
--

INSERT INTO `digitalpaintings` (`id`, `mobile`, `email`, `faces`, `paper`, `duration`, `image`, `created_at`, `updated_at`) VALUES
(12, '07013089044', 'blobyvj@gmail.com', 1, 'A4', 'Normal', '[{\"download_link\":\"digitalpaintings/December2019/1576767858.jpg\",\"original_name\":\"1576767858.jpg\"}]', '2019-12-19 09:34:18', '2019-12-19 09:34:18'),
(13, '07013089044', 'blobyvj@gmail.com', 1, 'A4', 'Normal', '[{\"download_link\":\"digitalpaintings/December2019/1576767923.jpg\",\"original_name\":\"1576767923.jpg\"}]', '2019-12-19 09:35:23', '2019-12-19 09:35:23'),
(14, '07013089044', 'blobyvj@gmail.com', 2, 'A4', 'Express', 'digitalpaintings\\December2019\\mFMSSJi9dzhYb6Q5Rtpo.jpg', '2019-12-19 09:35:55', '2019-12-19 09:35:55'),
(15, '07013089044', 'blobyvj@gmail.com', 1, 'A4', 'Normal', 'digitalpaintings/December2019/1576768037.jpg', '2019-12-19 09:37:17', '2019-12-19 09:37:17');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cat_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `cat_id`, `name`, `desc`, `created_at`, `updated_at`, `image`, `thumb`) VALUES
(1, 'actors', 'Test by vJ', 'Test by vJ', '2019-12-13 02:28:00', '2019-12-17 11:45:18', 'images\\December2019\\8mzuhHNCIVVctrXBchiJ.jpg', ''),
(2, 'actors', 'Act 1', 'Test', '2019-12-17 11:47:00', '2019-12-17 12:20:08', 'images\\December2019\\CDZrDZGsXCj6LeXCvo0J.jpg', 'images\\December2019\\5dL9ssoyO1hRi6VcgBey.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `image_categories`
--

DROP TABLE IF EXISTS `image_categories`;
CREATE TABLE IF NOT EXISTS `image_categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `image_categories`
--

INSERT INTO `image_categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Actors', 'actors', '2019-12-05 00:24:00', '2019-12-05 00:24:48'),
(2, 'Actress', 'actress', '2019-12-05 00:24:57', '2019-12-05 00:24:57'),
(3, 'Politicians', 'politicians', '2019-12-05 00:25:05', '2019-12-05 00:25:05');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-12-04 23:05:08', '2019-12-04 23:05:08'),
(2, 'front_end', '2019-12-04 23:09:48', '2019-12-04 23:09:48');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2019-12-04 23:05:08', '2019-12-04 23:05:08', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2019-12-04 23:05:08', '2019-12-04 23:05:08', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2019-12-04 23:05:08', '2019-12-04 23:05:08', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2019-12-04 23:05:08', '2019-12-04 23:05:08', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2019-12-04 23:05:08', '2019-12-04 23:05:08', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2019-12-04 23:05:08', '2019-12-04 23:05:08', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2019-12-04 23:05:08', '2019-12-04 23:05:08', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2019-12-04 23:05:08', '2019-12-04 23:05:08', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2019-12-04 23:05:08', '2019-12-04 23:05:08', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2019-12-04 23:05:08', '2019-12-04 23:05:08', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 8, '2019-12-04 23:05:14', '2019-12-04 23:05:14', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 6, '2019-12-04 23:05:16', '2019-12-04 23:05:16', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 7, '2019-12-04 23:05:16', '2019-12-04 23:05:16', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2019-12-04 23:05:19', '2019-12-04 23:05:19', 'voyager.hooks', NULL),
(15, 2, 'Home', '/home', '_self', NULL, '#000000', NULL, 1, '2019-12-04 23:11:41', '2019-12-04 23:12:36', NULL, ''),
(16, 2, 'Images', '#', '_self', NULL, '#000000', NULL, 2, '2019-12-04 23:12:03', '2019-12-04 23:12:36', NULL, ''),
(17, 2, 'Actors', '/images/actors', '_self', NULL, '#000000', 16, 1, '2019-12-04 23:12:33', '2019-12-04 23:12:36', NULL, ''),
(18, 2, 'Png', '#', '_self', NULL, '#000000', NULL, 3, '2019-12-04 23:12:49', '2019-12-04 23:13:11', NULL, ''),
(19, 2, 'Products', '/png/products', '_self', NULL, '#000000', 18, 1, '2019-12-04 23:13:08', '2019-12-04 23:13:11', NULL, ''),
(20, 2, 'Flick Titles', '#', '_self', NULL, '#000000', NULL, 15, '2019-12-04 23:13:41', '2019-12-04 23:13:41', NULL, ''),
(25, 1, 'Image Categories', '', '_self', 'voyager-categories', '#000000', NULL, 16, '2019-12-05 00:22:49', '2019-12-17 11:44:30', 'voyager.image-categories.index', 'null'),
(26, 1, 'Images', '', '_self', 'voyager-images', '#000000', NULL, 17, '2019-12-05 00:23:31', '2019-12-17 11:44:48', 'voyager.images.index', 'null'),
(27, 1, 'Png Categories', '', '_self', 'voyager-categories', '#000000', NULL, 18, '2019-12-13 08:21:00', '2019-12-19 02:43:27', 'voyager.png-categories.index', 'null'),
(28, 1, 'Pngs', '', '_self', 'voyager-images', '#000000', NULL, 19, '2019-12-13 09:14:44', '2019-12-19 02:43:41', 'voyager.pngs.index', 'null'),
(29, 1, 'Digitalpaintings', '', '_self', NULL, NULL, NULL, 20, '2019-12-19 08:33:25', '2019-12-19 08:33:25', 'voyager.digitalpaintings.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 2),
(4, '2016_01_01_000000_create_data_types_table', 2),
(5, '2016_05_19_173453_create_menu_table', 2),
(6, '2016_10_21_190000_create_roles_table', 2),
(7, '2016_10_21_190000_create_settings_table', 2),
(8, '2016_11_30_135954_create_permission_table', 2),
(9, '2016_11_30_141208_create_permission_role_table', 2),
(10, '2016_12_26_201236_data_types__add__server_side', 2),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 2),
(12, '2017_01_14_005015_create_translations_table', 2),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 2),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 2),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 2),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 2),
(17, '2017_08_05_000000_add_group_to_settings_table', 2),
(18, '2017_11_26_013050_add_user_role_relationship', 2),
(19, '2017_11_26_015000_create_user_roles_table', 2),
(20, '2018_03_11_000000_add_user_settings', 2),
(21, '2018_03_14_000000_add_details_to_data_types_table', 2),
(22, '2018_03_16_000000_make_settings_value_nullable', 2),
(23, '2016_01_01_000000_create_pages_table', 3),
(24, '2016_01_01_000000_create_posts_table', 3),
(25, '2016_02_15_204651_create_categories_table', 3),
(26, '2017_04_11_000000_alter_post_nullable_fields_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2019-12-04 23:05:17', '2019-12-04 23:05:17');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(2, 'browse_bread', NULL, '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(3, 'browse_database', NULL, '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(4, 'browse_media', NULL, '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(5, 'browse_compass', NULL, '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(6, 'browse_menus', 'menus', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(7, 'read_menus', 'menus', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(8, 'edit_menus', 'menus', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(9, 'add_menus', 'menus', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(10, 'delete_menus', 'menus', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(11, 'browse_roles', 'roles', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(12, 'read_roles', 'roles', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(13, 'edit_roles', 'roles', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(14, 'add_roles', 'roles', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(15, 'delete_roles', 'roles', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(16, 'browse_users', 'users', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(17, 'read_users', 'users', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(18, 'edit_users', 'users', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(19, 'add_users', 'users', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(20, 'delete_users', 'users', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(21, 'browse_settings', 'settings', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(22, 'read_settings', 'settings', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(23, 'edit_settings', 'settings', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(24, 'add_settings', 'settings', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(25, 'delete_settings', 'settings', '2019-12-04 23:05:09', '2019-12-04 23:05:09'),
(26, 'browse_categories', 'categories', '2019-12-04 23:05:14', '2019-12-04 23:05:14'),
(27, 'read_categories', 'categories', '2019-12-04 23:05:15', '2019-12-04 23:05:15'),
(28, 'edit_categories', 'categories', '2019-12-04 23:05:15', '2019-12-04 23:05:15'),
(29, 'add_categories', 'categories', '2019-12-04 23:05:15', '2019-12-04 23:05:15'),
(30, 'delete_categories', 'categories', '2019-12-04 23:05:15', '2019-12-04 23:05:15'),
(31, 'browse_posts', 'posts', '2019-12-04 23:05:16', '2019-12-04 23:05:16'),
(32, 'read_posts', 'posts', '2019-12-04 23:05:16', '2019-12-04 23:05:16'),
(33, 'edit_posts', 'posts', '2019-12-04 23:05:16', '2019-12-04 23:05:16'),
(34, 'add_posts', 'posts', '2019-12-04 23:05:16', '2019-12-04 23:05:16'),
(35, 'delete_posts', 'posts', '2019-12-04 23:05:16', '2019-12-04 23:05:16'),
(36, 'browse_pages', 'pages', '2019-12-04 23:05:16', '2019-12-04 23:05:16'),
(37, 'read_pages', 'pages', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(38, 'edit_pages', 'pages', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(39, 'add_pages', 'pages', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(40, 'delete_pages', 'pages', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(41, 'browse_hooks', NULL, '2019-12-04 23:05:19', '2019-12-04 23:05:19'),
(62, 'browse_image_categories', 'image_categories', '2019-12-05 00:22:49', '2019-12-05 00:22:49'),
(63, 'read_image_categories', 'image_categories', '2019-12-05 00:22:49', '2019-12-05 00:22:49'),
(64, 'edit_image_categories', 'image_categories', '2019-12-05 00:22:49', '2019-12-05 00:22:49'),
(65, 'add_image_categories', 'image_categories', '2019-12-05 00:22:49', '2019-12-05 00:22:49'),
(66, 'delete_image_categories', 'image_categories', '2019-12-05 00:22:49', '2019-12-05 00:22:49'),
(67, 'browse_images', 'images', '2019-12-05 00:23:31', '2019-12-05 00:23:31'),
(68, 'read_images', 'images', '2019-12-05 00:23:31', '2019-12-05 00:23:31'),
(69, 'edit_images', 'images', '2019-12-05 00:23:31', '2019-12-05 00:23:31'),
(70, 'add_images', 'images', '2019-12-05 00:23:31', '2019-12-05 00:23:31'),
(71, 'delete_images', 'images', '2019-12-05 00:23:31', '2019-12-05 00:23:31'),
(72, 'browse_png_categories', 'png_categories', '2019-12-13 08:21:00', '2019-12-13 08:21:00'),
(73, 'read_png_categories', 'png_categories', '2019-12-13 08:21:00', '2019-12-13 08:21:00'),
(74, 'edit_png_categories', 'png_categories', '2019-12-13 08:21:00', '2019-12-13 08:21:00'),
(75, 'add_png_categories', 'png_categories', '2019-12-13 08:21:00', '2019-12-13 08:21:00'),
(76, 'delete_png_categories', 'png_categories', '2019-12-13 08:21:00', '2019-12-13 08:21:00'),
(77, 'browse_pngs', 'pngs', '2019-12-13 09:14:43', '2019-12-13 09:14:43'),
(78, 'read_pngs', 'pngs', '2019-12-13 09:14:44', '2019-12-13 09:14:44'),
(79, 'edit_pngs', 'pngs', '2019-12-13 09:14:44', '2019-12-13 09:14:44'),
(80, 'add_pngs', 'pngs', '2019-12-13 09:14:44', '2019-12-13 09:14:44'),
(81, 'delete_pngs', 'pngs', '2019-12-13 09:14:44', '2019-12-13 09:14:44'),
(82, 'browse_digitalpaintings', 'digitalpaintings', '2019-12-19 08:33:24', '2019-12-19 08:33:24'),
(83, 'read_digitalpaintings', 'digitalpaintings', '2019-12-19 08:33:25', '2019-12-19 08:33:25'),
(84, 'edit_digitalpaintings', 'digitalpaintings', '2019-12-19 08:33:25', '2019-12-19 08:33:25'),
(85, 'add_digitalpaintings', 'digitalpaintings', '2019-12-19 08:33:25', '2019-12-19 08:33:25'),
(86, 'delete_digitalpaintings', 'digitalpaintings', '2019-12-19 08:33:25', '2019-12-19 08:33:25');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pngs`
--

DROP TABLE IF EXISTS `pngs`;
CREATE TABLE IF NOT EXISTS `pngs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cat_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pngs`
--

INSERT INTO `pngs` (`id`, `cat_id`, `name`, `desc`, `image`, `created_at`, `updated_at`, `edited`, `thumb`) VALUES
(1, 'politicians', 'Pol 1', 'Pol 1', 'pngs\\December2019\\J4MeLT6LtpwcawAMm7jS.jpg', '2019-12-13 09:50:36', '2019-12-13 09:50:36', NULL, ''),
(2, 'psd-test', 'psd 1', 'Test', 'pngs\\December2019\\dAqiy9xrWERImtzGShU2.jpg', '2019-12-17 11:50:00', '2019-12-17 12:18:35', NULL, ''),
(3, 'politicians', 'pngt', 'Png Edited Test', 'pngs\\December2019\\kbmRf7T6ByZM7HZx0Oxz.jpg', '2019-12-19 02:54:13', '2019-12-19 02:54:13', '1', ''),
(4, 'psd-test', 'Thumb Test', 'Thumb nail test', 'pngs\\December2019\\9H4pAOgTi9GAvrDEewSk.jpg', '2019-12-19 04:49:12', '2019-12-19 04:49:12', '1', 'pngs\\December2019\\rbEasGkzanXmotFjEeYJ.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `png_categories`
--

DROP TABLE IF EXISTS `png_categories`;
CREATE TABLE IF NOT EXISTS `png_categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `png_categories`
--

INSERT INTO `png_categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Politicians', 'politicians', '2019-12-13 08:21:23', '2019-12-13 08:21:23'),
(2, 'Psd Test', 'psd-test', '2019-12-13 08:26:47', '2019-12-13 08:26:47');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-12-04 23:05:16', '2019-12-04 23:05:16'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-12-04 23:05:16', '2019-12-04 23:05:16'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-12-04 23:05:16', '2019-12-04 23:05:16'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-12-04 23:05:16', '2019-12-04 23:05:16');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2019-12-04 23:05:08', '2019-12-04 23:05:08'),
(2, 'user', 'Normal User', '2019-12-04 23:05:09', '2019-12-04 23:05:09');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2019-12-04 23:05:17', '2019-12-04 23:05:17'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2019-12-04 23:05:18', '2019-12-04 23:05:18'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2019-12-04 23:05:18', '2019-12-04 23:05:18'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2019-12-04 23:05:18', '2019-12-04 23:05:18'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2019-12-04 23:05:18', '2019-12-04 23:05:18'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2019-12-04 23:05:18', '2019-12-04 23:05:18'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2019-12-04 23:05:18', '2019-12-04 23:05:18'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2019-12-04 23:05:18', '2019-12-04 23:05:18'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2019-12-04 23:05:18', '2019-12-04 23:05:18'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2019-12-04 23:05:18', '2019-12-04 23:05:18'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2019-12-04 23:05:18', '2019-12-04 23:05:18'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2019-12-04 23:05:18', '2019-12-04 23:05:18'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2019-12-04 23:05:18', '2019-12-04 23:05:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, NULL, 'venky', 'blobyvj@gmail.com', 'users/default.png', NULL, '$2y$10$ABSVNRABQY/bKWYCvvYyoeBUvyXr35Oo7uOWzpG7iFSSirwgylive', 'KBR7fNLs7f8HTEMwL6eqhOyiNPvAkeqPlDXk0D2xbMej31KXB8uZbCbQ27PI', NULL, '2019-12-04 22:54:39', '2019-12-04 22:54:39'),
(2, 1, 'Admin', 'admin@garuda.test', 'users/default.png', NULL, '$2y$10$f1kzc94ZDl2HvuFdqysEbunYbkOpH1e82S94Qy6FhEXF/8p2xld62', NULL, NULL, '2019-12-04 23:08:35', '2019-12-04 23:08:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
