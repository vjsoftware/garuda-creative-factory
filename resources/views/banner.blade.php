<!DOCTYPE html>

<!-- Mirrored from html.webinane.com/picstock/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 06:08:22 GMT -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Garuda Creative Factory</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{ config('app.url') }}/css/icons.css">
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/style.css" />
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/responsive.css" />
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/color.css" />

	<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/extralayers.css" media="screen" />	
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/settings.css" media="screen" />

    <style>
        ul li {
            list-style: none;
            float: left;
        }
.product-image-gallery {
  background-color: #fefefe;
  /* padding: 0.5rem; */
}

.pdp-product-image {
  margin-bottom: 20px;
}

.product-thumbs a {
  margin-left: 8px;
  margin-right: 8px;
  padding: 0 !important;
}

.product-thumbs a img {
  display: inline-block;
  max-width: 100%;
  margin-bottom: 1rem;
  border: solid 4px #fefefe;
  border-radius: 0;
  box-shadow: 0 0 0 1px rgba(10, 10, 10, 0.2);
  line-height: 0;
  border-radius: 3px;
  width: 50px;
  height: 50px;
  border: none;
}


    </style>
</head>
<body>
<div class="theme-layout">
	@include('layouts/navbar')	

	
<section>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-12 column">
						<div class="detail-page">
							<form action="{{ route('bannerStore') }}" method="POST" enctype="multipart/form-data">
							<div class="single-img-detail">
                                <h2>(Banner) Please fill the Following fields:</h2>
                                <hr>
                                <span>Options</span>
                                <br>
                                <br>
                                <div class="form-group row">
                                    <label for="width" class="col-sm-5 col-form-label">Width <span class="text-warning">*</span>:</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="width" id="" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="height" class="col-sm-5 col-form-label">Height <span class="text-warning">*</span>:</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="height" id="" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Size:</label>
                                    <div class="col-sm-7">
										@csrf
                                    <select name="faces" id="size" class="form-control" required>
                                            <option value=""> Select One </option>
                                            <option value="Inch"> Inch </option>
                                            <option value="Feet"> Feet </option>
                                        </select>
                                    </div>
                                </div>
								<br>
								<br>
                                <div class="form-group row">
                                    <div class="col-md-5">
                                        <label for="staticEmail" class="col-sm-5 col-form-label">Type <span class="text-warning">*</span>:</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="type" id="" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <label for="staticEmail" class="col-sm-5 col-form-label">Layout <span class="text-warning">*</span>:</label>
                                        <div class="col-sm-7">
                                            <input type="file" name="layout" id="" class="form-control" required>
                                        </div>
                                    </div>
                                    
                                </div>
                                <br>
                                <div class="form-group row">
                                    <div class="col-md-5">
                                        <label for="staticEmail" class="col-sm-5 col-form-label">Email <span class="text-warning">*</span>:</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="email" id="" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <label for="staticEmail" class="col-sm-5 col-form-label">Attachment's <span class="text-warning">*</span>:</label>
                                        <div class="col-sm-7">
                                            <input type="file" name="attachment" id="" class="form-control" required>
                                        </div>
                                    </div>
                                    
                                </div>
                                <br>
                                <div class="form-group row">
                                    <div class="col-md-5">
                                        <label for="staticEmail" class="col-sm-5 col-form-label">Notes <span class="text-warning">*</span>:</label>
                                        <div class="col-sm-7">
                                            <textarea name="notes" id="" rows="5" class="form-control" required></textarea>
                                            {{-- <input type="text" name="mobile" id="" class="form-control" required> --}}
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        {{-- <label for="staticEmail" class="col-sm-5 col-form-label">Attachment's <span class="text-warning">*</span>:</label> --}}
                                        <div class="col-sm-7">
                                            <input type="submit" name="mobile" id="" class="btn btn-primary">
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-md-5"></div>
                                <div class="col-md-5 offset-md-2">
                                    <h1>Contact For Update</h1>
                                    <a data-letters="Whats App" class="theme-btn" style="background: #2159d8" href="https://api.whatsapp.com/send?phone=919966775234&text=I%27m%20interested%20in%20your%20updates#" target="_blank" title="Whats App">Whats App</a>
								<br><br>
								{{-- <input type="submit"  name="Order Now" id="" value> --}}
								<button class="theme-btn" type="submit">Order Now</button>
								{{-- <a data-letters="Order Now" class="theme-btn" href="#" title="">Order Now</a> --}}
                                </div>
                                
							</div>
						</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


	@include('layouts/footer')

	@include('auth/popup')

	<div class="wishlist-btn"><a href="{{ config('app.url') }}/wishlist.html" title=""><i class="fa fa-heart"></i></a></div>
	

	<script src="{{ config('app.url') }}/js/jquery.min.js" type="text/javascript"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
	<script type="text/javascript" src="{{ config('app.url') }}/js/revolution/jquery.themepunch.tools.min.js"></script>   
	<script type="text/javascript" src="{{ config('app.url') }}/js/revolution/jquery.themepunch.revolution.min.js"></script>

	<script src="{{ config('app.url') }}/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/enscroll-0.5.2.min.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/jquery.scrolly.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/jquery.isotope.min.js"></script>
	<script src="{{ config('app.url') }}/js/isotope-initialize.js"></script>
	<script src="{{ config('app.url') }}/js/script.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function(){
			jQuery('.tp-banner').show().revolution({
				delay:15000,
				startwidth:1170,
				startheight:540,
				autoHeight:"off",
				navigationType:"none",
				hideThumbs:10,
				fullWidth:"on",
				fullScreen:"on",
				fullScreenOffsetContainer:""
			});	

	});
    </script>
    <script>
        // $('.sim-thumb').on('click', function() {
        //     $('#main-product-image').attr('src', $(this).data('image'));
        // })
    </script>
    <script>
        // $('select').change(function() {
		// 	var faces = parseInt($('#faces').val());
        //     var paper = parseInt($('#paper').val());
		// 	var duration = parseInt($('#duration').val());
		// 	var makingPrice = parseInt(490);
		// 	var price = makingPrice + faces + paper + duration;
            
        //     $('#price').html(price);

        // })
    </script>

</body>

<!-- Mirrored from html.webinane.com/picstock/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 06:08:42 GMT -->
</html>