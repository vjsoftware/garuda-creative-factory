<!DOCTYPE html>
<!-- Mirrored from html.webinane.com/picstock/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 06:08:22 GMT -->
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <title>Garuda Creative Factory</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="description" content="" />
   <meta name="keywords" content="" />
   <!-- Styles -->
   <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
   <link rel="stylesheet" href="css/icons.css">
   <link rel="stylesheet" type="text/css" href="css/style.css" />
   <link rel="stylesheet" type="text/css" href="css/responsive.css" />
   <link rel="stylesheet" type="text/css" href="css/color.css" />
   <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
   <link rel="stylesheet" type="text/css" href="css/extralayers.css" media="screen" />
   <link rel="stylesheet" type="text/css" href="css/settings.css" media="screen" />
</head>
<body>
   <div class="theme-layout">
      @include('layouts/navbar')	
	  @include('search')
      @include('packages')
      @include('topcategories')
   </div>
   @include('layouts/footer')
   @include('auth/popup')
   
   <script src="js/jquery.min.js" type="text/javascript"></script>
   <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
   <script type="text/javascript" src="js/revolution/jquery.themepunch.tools.min.js"></script>   
   <script type="text/javascript" src="js/revolution/jquery.themepunch.revolution.min.js"></script>
   <script src="js/bootstrap.min.js" type="text/javascript"></script>
   <script src="js/enscroll-0.5.2.min.js" type="text/javascript"></script>
   <script src="js/jquery.scrolly.js" type="text/javascript"></script>
   <script src="js/jquery.isotope.min.js"></script>
   <script src="js/isotope-initialize.js"></script>
   <script src="js/script.js" type="text/javascript"></script>
   <script type="text/javascript">
      $(document).ready(function(){
      		jQuery('.tp-banner').show().revolution({
      			delay:15000,
      			startwidth:1170,
      			startheight:540,
      			autoHeight:"off",
      			navigationType:"none",
      			hideThumbs:10,
      			fullWidth:"on",
      			fullScreen:"on",
      			fullScreenOffsetContainer:""
      		});	
      
      });
   </script>
</body>
<!-- Mirrored from html.webinane.com/picstock/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 06:08:42 GMT -->
</html>