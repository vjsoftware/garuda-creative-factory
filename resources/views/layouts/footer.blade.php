<footer>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-2 column">
						<div class="widget">
							<h4 class="widget-title">GCF</h4>
							<ul>
								<li><a href="{{ config('app.url') }}/" title="">Home</a></li>
								<li><a href="{{ config('app.url') }}/images/actors" title="">Images</a></li>
								<li><a href="{{ config('app.url') }}/png/actors" title="">Png's</a></li>
								<li><a href="#" title="">Contact Us</a></li>
							</ul>
						</div><!-- Widget -->
					</div>
					<div class="col-md-2 column">
						<div class="widget">
							<h4 class="widget-title">Products</h4>
							<ul>
								<li><a href="{{ route('painting') }}" title="">Digital Painting</a></li>
								<li><a href="{{ route('art') }}" title="">Photo to Art</a></li>
								<li><a href="{{ route('sketch') }}" title="">Pencil Sketch</a></li>
								<li><a href="{{ route('caricature') }}" title="">Caricature</a></li>
								<li><a href="{{ route('restoration') }}" title="">Image Restoration</a></li>
								<li><a href="{{ route('banner') }}" title="">Banner's</a></li>
								<li><a href="{{ route('std') }}" title="">Save the Date</a></li>
							</ul>
						</div><!-- Widget -->
					</div>
					<div class="col-md-2 column">
						<div class="widget">
							<h4 class="widget-title">Ways To Pay</h4>
							<ul>
								<li><a href="#" title="">Cart</a></li>
								<li><a href="#" title="">Checkout</a></li>
								<li><a href="#" title="">Wishlist</a></li>
								<li><a href="#" title="">Plans & Pricing</a></li>
								<li><a href="#" title="">Coorporate Accounts</a></li>
							</ul>
						</div><!-- Widget -->
					</div>
					<div class="col-md-3 column">
						<div class="widget">
							<h4 class="widget-title">Free Images</h4>
							<div class="images-widget">
								<a href="#" title=""><img src="images/resource/image-widget1.jpg" alt="" /></a>
								<a href="#" title=""><img src="images/resource/image-widget2.jpg" alt="" /></a>
								<a href="#" title=""><img src="images/resource/image-widget3.jpg" alt="" /></a>
								<a href="#" title=""><img src="images/resource/image-widget4.jpg" alt="" /></a>
								<a href="#" title=""><img src="images/resource/image-widget5.jpg" alt="" /></a>
								<a href="#" title=""><img src="images/resource/image-widget6.jpg" alt="" /></a>
							</div>
						</div>
					</div>
					<div class="col-md-3 column">
						<div class="widget">
							<h4 class="widget-title">Follow Us On</h4>
							<div class="social-widget">
								{{-- <p>Search for images anywhere using our award-winning iPad apps.</p> --}}
								<ul>
									<li><a style="background:#0070cf;" href="#" title=""><i class="fa fa-facebook"></i></a></li>
									<li><a style="background:#be3100;" href="#" title=""><i class="fa fa-google-plus"></i></a></li>
									<li><a style="background:#d900a3;" href="#" title=""><i class="fa fa-flickr"></i></a></li>
									<li><a style="background:#e04c86;" href="#" title=""><i class="fa fa-dribbble"></i></a></li>
									<li><a style="background:#fe3400;" href="#" title=""><i class="fa fa-reddit"></i></a></li>
								</ul>
								{{-- <form>
									<input type="text" placeholder="Subscribe" />
									<button><i class="ti-arrow-right"></i></button>
								</form>								 --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div class="bottom-footer">
		<div class="container"><p>© {{ Date('Y') }} <a href="https://garudacreativefactory.com">Garuda Creative Factory</a>, Inc. All rights reserved.</p></div>		
	</div>