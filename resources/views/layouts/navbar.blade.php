<header>
		<div class="container">
			<div class="header-container">
				<nav>
					<ul>
						<li><a href="{{ config('app.url') }}" title="">Home</a></li>
						<li><a href="#" title="">Images</a>
							<ul>
								@php
									$imageItems = DB::select("SELECT * FROM image_categories");
								@endphp
								@foreach ($imageItems as $item)
									<li><a href="/images/{{ $item->slug }}" title="">{{ $item->name }}</a></li>
								@endforeach
							</ul>
						</li>
						<li><a href="#" title="">Png</a>
							<ul>
								@php
									$pngItems = DB::select("SELECT * FROM `png_categories`");
									@endphp
								@foreach ($pngItems as $item)
									<li><a href="/png/{{ $item->slug }}" title="">{{ $item->name }}</a></li>
								@endforeach
							</ul>
						</li>
						<li><a href="#" title="">Products</a>
							<ul>
								<li><a href="{{ route('painting') }}" title="">Digital Painting</a></li>
								<li><a href="{{ route('art') }}" title="">Photo to Art</a></li>
								<li><a href="{{ route('sketch') }}" title="">Pencil Sketch</a></li>
								<li><a href="{{ route('caricature') }}" title="">Caricature</a></li>
								<li><a href="{{ route('restoration') }}" title="">Restoration</a></li>
								<li><a href="{{ route('banner') }}" title="">Banner</a></li>
							</ul>
						</li>
						<li><a href="{{ route('std') }}/" title="">Save the Date</a></li>
					</ul>
				</nav><!-- Navigation -->
				<div class="header-links">
					<ul>
						@if (Auth::check())
							<li>
								{{-- <a class="call-reg" href="{{  }}" title="">Logout</a> --}}
							
							<a class="call-reg" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
									</form>
							</li>
						@else
							<li><a class="call-reg login-popup" href="#" title="">Log in</a></li>
							<li><a class="call-reg signup-popup" href="#" title="">Sign up</a></li>
						@endif
						{{-- <li><a href="cart.html" title="">Cart (0)</a></li> --}}
					</ul>
				</div><!-- Header Links -->
			</div>
		</div>
    </header><!-- Header -->
    
    <div class="responsive-header">
		<div class="responsive-bar">
			<span class="open-menu"><i data-target="menu" class="fa fa-align-center"></i></span>
			<div class="logo"><a href="#" title=""><img src="images/logo.png" alt="" /></a></div><!-- Logo -->
			<span class="open-links"><i data-target="links" class="fa fa-arrow-right"></i></span>
		</div>
		<div class="responsive-links menu">
			<ul>
				<li><a href="index-2.html" title="">Home</a></li>
				<li class="menu-item-has-children"><a href="#" title="">Images</a>
					<ul>
						<li><a href="images-list.html" title="">Images List</a></li>
						<li><a href="images-list-sidebar.html" title="">Images List With Sidebar</a></li>
						<li><a href="single-page.html" title="">Single Image Page 1</a></li>
						<li><a href="single-page2.html" title="">Single Image Page 2</a></li>
						<li><a href="download-history.html" title="">Download History</a></li>
						<li><a href="freedownloads.html" title="">Free Downloads</a></li>
					</ul>
				</li>
				<li class="menu-item-has-children"><a href="#" title="">Cart</a>
					<ul>
						<li><a href="cart.html" title="">Cart</a></li>
						<li><a href="checkout.html" title="">Checkout</a></li>
						<li><a href="wishlist.html" title="">Wishlist</a></li>
					</ul>
				</li>
				<li class="menu-item-has-children"><a href="#" title="">More Pages</a>
					<ul>
						<li class="menu-item-has-children"><a href="#" title="">Blog</a>
							<ul>
								<li><a href="blog.html" title="">Blog List</a></li>
								<li><a href="blog-single.html" title="">Blog Single</a></li>
								<li><a href="blog-single-sidebar.html" title="">Blog Single With Sidebar</a></li>
							</ul>
						</li>
						<li><a href="affiliations.html" title="">Affiliations</a></li>
						<li><a href="account-detail.html" title="">Account Details</a></li>
						<li><a href="price-package.html" title="">Price Package</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="responsive-links other">
			<ul>
				<li><a href="#" title=""><i class="fa fa-shopping-cart"></i>Cart (0)</a></li>
				<li><a class="call-reg login-popup" href="#" title="">Log in</a></li>
				<li><a class="call-reg signup-popup" href="#" title="">Sign up</a></li>
			</ul>
		</div>
	</div><!-- Responsive Header -->