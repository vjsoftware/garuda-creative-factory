<!DOCTYPE html>

<!-- Mirrored from html.webinane.com/picstock/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 06:08:22 GMT -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Garuda Creative Factory</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{ config('app.url') }}/css/icons.css">
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/style.css" />
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/responsive.css" />
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/color.css" />

	<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/extralayers.css" media="screen" />	
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/settings.css" media="screen" />

</head>
<body>
<div class="theme-layout">
	@include('layouts/navbar')	
    <section>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-12 column">
						<div class="page-top-search">
							<div class="main-search fadeInUp">
								<form>
									<input type="text" placeholder="Search Here Your Keywords">
									<button><i class="fa fa-search"></i></button>
								</form>
							</div><!-- Main Search -->
						</div><!-- Page Top Search Bar -->
					</div>
				</div>
			</div>
		</div>
	</section>

    @php
        $slug = $png['cat_id'];
        $images = DB::SELECT("SELECT * FROM `pngs` WHERE `cat_id` = '$slug'")
    @endphp
	
<section>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-12 column">
						<div class="detail-page">
							<div class="single-img"><img src="{{ Voyager::image($png['thumb']) }}" alt=""></div>
							<div class="single-img-detail">
								<h2>{{ $png['name'] }}:</h2>
								<span>{{ $png['desc'] }}.</span>
								{{-- {{ session('buyId') }} --}}
								

								@if (Auth::check())
									@php
										$userId = Auth::user()->id;
										$now = Carbon\Carbon::now();
										$findMemberShip = DB::table('users')->where('id', $userId)->whereDate('pakage', '>', "$now")->count();
										// dd($findMemberShip);
									@endphp
									@if ($findMemberShip == 0)
										@if ($png['edited'] == 1)
											<button data-letters="Buy Now" class="theme-btn" id="ajaxbuypngpricebutton" onclick="ajaxbuypngpricebutton('{{ $png["id"] }}')">Buy Now</button>                                    
										@else
											<button data-letters="Buy Now" class="theme-btn" id="ajaxbuypngbutton" onclick="ajaxbuypngbutton('{{ $png["id"] }}')">Buy Now</button>                                    
										@endif
									@else
									@php
										$created = new Carbon\Carbon(Auth::user()->pakage);
										$now = Carbon\Carbon::now();
										$difference = ($created->diff($now)->days < 1)
											? 'today'
											: $created->diffInDays($now);
									@endphp
									<br><br><br>
									<h5 class="text-warning">You have {{ $difference }} Days of Premium Account</h5>
                                    <a data-letters="Download" class="theme-btn" download  href="{{ Voyager::image($png['image']) }}">Download</a>
									@endif
								@else
									@if ($png['edited'] == 1)
										<button data-letters="Buy Now" class="theme-btn" id="ajaxbuypngpricebutton" onclick="ajaxbuypngpricebutton('{{ $png["id"] }}')">Buy Now</button>                                    
									@else
										<button data-letters="Buy Now" class="theme-btn" id="ajaxbuypngbutton" onclick="ajaxbuypngbutton('{{ $png["id"] }}')">Buy Now</button>                                    
									@endif
								@endif





                                
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


	@include('layouts/footer')


	@include('auth/popup')

	<div class="wishlist-btn"><a href="{{ config('app.url') }}/wishlist.html" title=""><i class="fa fa-heart"></i></a></div>
	

	<script src="{{ config('app.url') }}/js/jquery.min.js" type="text/javascript"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
	<script type="text/javascript" src="{{ config('app.url') }}/js/revolution/jquery.themepunch.tools.min.js"></script>   
	<script type="text/javascript" src="{{ config('app.url') }}/js/revolution/jquery.themepunch.revolution.min.js"></script>

	<script src="{{ config('app.url') }}/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/enscroll-0.5.2.min.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/jquery.scrolly.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/jquery.isotope.min.js"></script>
	<script src="{{ config('app.url') }}/js/isotope-initialize.js"></script>
	<script src="{{ config('app.url') }}/js/script.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function(){
			jQuery('.tp-banner').show().revolution({
				delay:15000,
				startwidth:1170,
				startheight:540,
				autoHeight:"off",
				navigationType:"none",
				hideThumbs:10,
				fullWidth:"on",
				fullScreen:"on",
				fullScreenOffsetContainer:""
			});	

	});
	</script>
	<script>
		function ajaxbuypngbutton(id) {
			// console.log(id);
			var data = {buyId: id}
			$.ajax({
			type:'POST',
			url:'/ajaxbuypng',
			data: data,
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success:function(data) {
				if (data = 'sucess') {
					window.location.href = "https://www.payumoney.com/paybypayumoney/#/A2C4CCC85A1078CDA54E60EE51930DA5";
				}
				// alert(data);
				// $("#msg").html(data.msg);
			}
		});
		}
		function ajaxbuypngpricebutton(id) {
			// console.log(id);
			var data = {buyId: id}
			$.ajax({
			type:'POST',
			url:'/ajaxbuypng',
			data: data,
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success:function(data) {
				if (data = 'sucess') {
					window.location.href = "https://www.payumoney.com/paybypayumoney/#/FCA8DE45E75F9B9A5EC610FBAFF31436";
				}
				// alert(data);
				// $("#msg").html(data.msg);
			}
		});
		}
</script>
</body>

</html>