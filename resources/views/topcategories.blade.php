<section>
		<div class="block remove-gap">
			<div class="row ">
				<div class="col-md-12 column">
					<div class="title center">
						<h2>Top Categories</h2>
						<i>You can select from the key</i>
					</div>

					<div class="options">
						<div class="option-combo">
							<ul id="filter" class="option-set" data-option-key="filter">
								<li><a href="#showall" data-option-value="*" class="selected">Show all</a></li>
								@php
									$categories = DB::SELECT("SELECT * FROM image_categories");
								@endphp
								@foreach ($categories as $item)
									<li><a href="#{{ $item->slug }}" data-option-value=".{{ $item->slug }}">{{ $item->name }}</a></li>	
								@endforeach
 							</ul>
						</div>
					</div><!-- FILTER BUTTONS -->

					<div class="row narrow">
						<div class="masonary">
							@php
								$images = DB::SELECT("SELECT * FROM `images` ORDER BY `id` DESC LIMIT 12");
							@endphp
							@foreach ($images as $item)
								<div class="col-md-3 {{ $item->cat_id }}">
										<div class="image-box">
										<a href="{{ config('app.url').'/images/'.$item->cat_id.'/'.$item->id }}" title=""><img src="{{ Voyager::image($item->thumb) }}" alt="" /></a>
											<span class="image-name"># 231366088</span>
											<span class="image-license">License :    x - xxl</span>
											<div class="image-links">
												<a href="#" title=""><i class="fa fa-shopping-cart"></i></a>
												<a href="#" title=""><i class="fa fa-heart"></i></a>
											</div>
										</div>
									</div>	
							@endforeach
							
						</div>
					</div>	
				</div>
			</div>
		</div>
	</section>