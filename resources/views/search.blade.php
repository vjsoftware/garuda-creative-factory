
<section>
		<div class="block no-padding">
			<div class="row">
				<div class="col-md-12 column">
					<div class="main-section">
						<div class="image-slider">
							<div class="tp-banner">
								<ul>	
									<li data-transition="slidedown  cube-vertical"  data-masterspeed="500">
										<img src="images/resource/slide-image1.jpg"  alt="" data-duration="120" data-ease="Power0.easeInOut">
									</li><!-- SLIDE  -->
									<li data-transition="slideleft cube-horizontal"  data-masterspeed="500">
										<img src="images/resource/slide-image2.jpg"  alt="" data-duration="120" data-ease="Power0.easeInOut ">
									</li><!-- SLIDE  -->
								</ul>
							</div>
						</div><!-- Image Slider -->
						<div class="welcome-overlay">
							<div class="welcome-inner">
								<div class="logo"><a href="{{ config('app.url') }}" title=""><img src="images/garuda_logo.png" alt="" height="200px"/></a></div><!-- Logo -->
								<div class="main-tabber">
									{{-- <ul class="custom-tabs">
										<li class="active"><a href="#" data-name="all-search" title="">all Search</a></li>
										<li><a href="#"  data-name="category" title=""><i class="fa fa-diamond"></i> Images by Category</a></li>
									</ul> --}}
									<!-- Main Tabber Buttons -->
									<div class="custom-content">
										<div data-id="all-search" class="tab-detail active">
											<div class="main-search fadeInUp">
												{{-- <div class="col-md-2 text-white" style="color: #FFF !important"><input type="checkbox" name="type">  Images</div>
												<div class="col-md-2"><input type="checkbox" name="type">  Png's</div> --}}
												
												<form action="{{ route('search') }}" method="POST">
													@csrf
													<div class="checkbox" style="color: #FFF !important; float: left; margin-top: -35px;">
													<label><input type="checkbox" name="type" value="1">Image's &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</label>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<label><input type="checkbox" name="type" value="2">Png's</label>
												</div>
													<input type="text" name="search" placeholder="Search Here Your Keywords" required style="color: #000">
													<button><i class="fa fa-search"></i></button>
												</form>
											</div><!-- Main Search -->
										</div>
										<div data-id="category" class="tab-detail">
											<div class="images-category">
												<ul class="col-md-3">
													<li><a href="images-list.html" title="">Abstract</a></li>
													<li><a href="images-list.html" title="">Animals / Wildlife</a></li>
													<li><a href="images-list.html" title="">The Arts</a></li>
													<li><a href="images-list.html" title="">Backgrounds / Textures</a></li>
													<li><a href="images-list.html" title="">Beauty / Fashion</a></li>
												</ul>
												<ul class="col-md-3">
													<li><a href="images-list.html" title="">Buildings / Landmarks</a></li>
													<li><a href="images-list.html" title="">Backgrounds / Textures</a></li>
													<li><a href="images-list.html" title="">Beauty / Fashion</a></li>
													<li><a href="images-list.html" title="">Animals / Wildlife</a></li>
													<li><a href="images-list.html" title="">The Arts</a></li>
												</ul>
												<ul class="col-md-3">
													<li><a href="images-list.html" title="">The Arts</a></li>
													<li><a href="images-list.html" title="">Backgrounds / Textures</a></li>
													<li><a href="images-list.html" title="">Beauty / Fashion</a></li>
													<li><a href="images-list.html" title="">Animals / Wildlife</a></li>
													<li><a href="images-list.html" title="">The Arts</a></li>
												</ul>
												<ul class="col-md-3">
													<li><a href="images-list.html" title="">The Arts</a></li>
													<li><a href="images-list.html" title="">Backgrounds / Textures</a></li>
													<li><a href="images-list.html" title="">Beauty / Fashion</a></li>
													<li><a href="images-list.html" title="">Animals / Wildlife</a></li>
													<li><a href="images-list.html" title="">The Arts</a></li>
												</ul>
											</div><!-- Images Category -->
										</div>
									</div>
								</div><!-- Main Tabber -->
							</div>
						</div>
						<!-- Welcome Overlay -->
						@php
							$imagesCount = DB::SELECT("SELECT count(*) as count from `images`");
							$pngsCount = DB::SELECT("SELECT count(*) as count from `pngs`");
							$usersCount = DB::SELECT("SELECT count(*) as count from `users`");
						// dd($imagesCount);
						@endphp
						<div class="site-info-bar">
							<div class="info-box">
								<i class="fa fa-upload"></i>
								<span>Total Images</span>
								<strong>{{ $imagesCount[0]->count + $pngsCount[0]->count }}</strong>
							</div><!-- Info Box -->
							<div class="info-box">
								<i class="fa fa-user"></i>
								<span>Our Users</span>
								<strong>{{ $usersCount[0]->count }}</strong>
							</div><!-- Info Box -->
							<div class="info-box">
								<i class="fa fa-phone"></i>
								<span>Call Us</span>
								<strong>----------</strong>
							</div><!-- Info Box -->
						</div><!-- Site Info Bar -->
					</div><!-- Main Section -->
				</div>
			</div>
		</div>
	</section>