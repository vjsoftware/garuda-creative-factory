<!DOCTYPE html>

<!-- Mirrored from html.webinane.com/picstock/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 06:08:22 GMT -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Garuda Creative Factory</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{ config('app.url') }}/css/icons.css">
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/style.css" />
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/responsive.css" />
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/color.css" />

	<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/extralayers.css" media="screen" />	
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/settings.css" media="screen" />

</head>
<body>
<div class="theme-layout">
	@include('layouts/navbar')
    
	

	<section>
        <br><br>
        <br><br>
		<div class="block remove-gap">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="title center">
                        <h2>Save the Date</h2>
						</div>
						<div class="filter-bar">
							<div class="grid-select">
								<a class="three-col" href="#" title=""><img src="images/three-column.png" alt="" /></a>
								<a class="four-col" href="#" title=""><img src="images/four-column.png" alt="" /></a>
							</div>
							
						</div>
						<div class="images-list">
                                <br><br>
							<div class="row scroll" data-ui="jscroll-default">
                                @php
                                        $images = DB::SELECT("SELECT * FROM `stdviews`")
                                        @endphp
                                    @foreach ($images as $item)
                                            <div class="col-md-3">
                                        <div class="image-download">
                                            <a href="{{ config('app.url').'/save-the-date'.'/'.$item->id }}">
                                                <img src="{{ Voyager::image($item->image) }}" alt="" />
										<div class="image-bar">
											<div class="cat"><a href="{{ config('app.url').'/save-the-date'.'/'.$item->id }}">{{ $item->name }}</a></div>
											{{-- <span>12 Credits</span> --}}
										</div>
                                            </a>
										
										{{-- <div class="image-bar bottom">
											<div class="image-links">
												<a href="#" title=""><i class="fa fa-shopping-cart"></i></a>
												<a href="#" title=""><i class="fa fa-heart"></i></a>
											</div>
											<span>License : x - xxl</span>
										</div> --}}
									</div><!-- Image -->
								</div>
                                    @endforeach
									
								
								</div>
				                {{-- <div class="next"><a href="images-list2.html"><i class="ti-reload"></i><span>1 to 399</span></a></div> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


	@include('layouts/footer')


	@include('auth/popup')

	<div class="wishlist-btn"><a href="{{ config('app.url') }}/wishlist.html" title=""><i class="fa fa-heart"></i></a></div>
	

	<script src="{{ config('app.url') }}/js/jquery.min.js" type="text/javascript"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
	<script type="text/javascript" src="{{ config('app.url') }}/js/revolution/jquery.themepunch.tools.min.js"></script>   
	<script type="text/javascript" src="{{ config('app.url') }}/js/revolution/jquery.themepunch.revolution.min.js"></script>

	<script src="{{ config('app.url') }}/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/enscroll-0.5.2.min.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/jquery.scrolly.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/jquery.isotope.min.js"></script>
	<script src="{{ config('app.url') }}/js/isotope-initialize.js"></script>
	<script src="{{ config('app.url') }}/js/script.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function(){
			jQuery('.tp-banner').show().revolution({
				delay:15000,
				startwidth:1170,
				startheight:540,
				autoHeight:"off",
				navigationType:"none",
				hideThumbs:10,
				fullWidth:"on",
				fullScreen:"on",
				fullScreenOffsetContainer:""
			});	

	});
	</script>

</body>

<!-- Mirrored from html.webinane.com/picstock/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 06:08:42 GMT -->
</html>