@php
	use App\Stdview;
@endphp
<!DOCTYPE html>

<!-- Mirrored from html.webinane.com/picstock/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 06:08:22 GMT -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Garuda Creative Factory</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{ config('app.url') }}/css/icons.css">
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/style.css" />
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/responsive.css" />
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/color.css" />

	<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/extralayers.css" media="screen" />	
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/settings.css" media="screen" />

    <style>
        ul li {
            list-style: none;
            float: left;
        }
.product-image-gallery {
  background-color: #fefefe;
  /* padding: 0.5rem; */
}

.pdp-product-image {
  margin-bottom: 20px;
}

.product-thumbs a {
  margin-left: 8px;
  margin-right: 8px;
  padding: 0 !important;
}

.product-thumbs a img {
  display: inline-block;
  max-width: 100%;
  margin-bottom: 1rem;
  border: solid 4px #fefefe;
  border-radius: 0;
  box-shadow: 0 0 0 1px rgba(10, 10, 10, 0.2);
  line-height: 0;
  border-radius: 3px;
  width: 50px;
  height: 50px;
  border: none;
}


    </style>
</head>
<body>
<div class="theme-layout">
	@include('layouts/navbar')	

	
<section>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-12 column">
						<div class="detail-page">
							<div class="single-img">
                                <div class="row align-center">
                                <div class="product-image-gallery">
									@php
										$video = Stdview::find($id);
									@endphp
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/{{ $video['video'] }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    
                                </div>
                                </div>
							</div>
							<form action="{{ route('stdStore') }}" method="POST" enctype="multipart/form-data">
							<div class="single-img-detail">
                                <h2>Save the Date:</h2>
                                <hr>
                                <span>Options</span>
                                <br>
								<br>
								<input type="hidden" name="video" value="{{ $video['video'] }}">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Family Name:</label>
                                    <div class="col-sm-7">
										@csrf
                                    <input type="text" name="fname" id="fname" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Bride Name:</label>
                                    <div class="col-sm-7">
                                    <input type="text" name="bname" id="bname" class="form-control" required>
										
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Groom Name:</label>
                                    <div class="col-sm-7">
                                    <input type="text" name="gname" id="gname" class="form-control" required>
                                    </div>
								</div>
								<br>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Venue Date <span class="text-warning">*</span>:</label>
                                    <div class="col-sm-7">
                                        <input type="date" name="date" id="date" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Venue Time <span class="text-warning">*</span>:</label>
                                    <div class="col-sm-7">
                                        <input type="time" name="time" id="time" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Venue At <span class="text-warning">*</span>:</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="venue" id="venue" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Image:</label>
                                    <div class="col-sm-7">
                                        <input type="file" name="image" id="image" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Image 2:</label>
                                    <div class="col-sm-7">
                                        <input type="file" name="image2" id="image2" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">E@Mail or WhatsApp Number <span class="text-warning">*</span>:</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="email" id="email" class="form-control" required>
                                    </div>
                                </div>

                                {{-- <h1>Contact For Update</h1> --}}
                                {{-- <a data-letters="Whats App" class="theme-btn" style="background: #2159d8" href="https://api.whatsapp.com/send?phone=919966775234&text=I%27m%20interested%20in%20your%20updates#" target="_blank" title="Whats App">Whats App</a> --}}
                                <br><br>
                                {{-- <button data-letters="Pay" class="theme-btn" id="ajaxbuybutton" onclick="buybutton(this)" type="button">Pay</button> --}}
								{{-- <input type="submit" name="Pay" id="" value> --}}
								<button class="theme-btn" type="submit">Pay</button>
								{{-- <a data-letters="Order Now" class="theme-btn" href="#" title="">Order Now</a> --}}
							</div>
						</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


	@include('layouts/footer')

	@include('auth/popup')

	{{-- <div class="wishlist-btn"><a href="{{ config('app.url') }}/wishlist.html" title=""><i class="fa fa-heart"></i></a></div> --}}
	

	<script src="{{ config('app.url') }}/js/jquery.min.js" type="text/javascript"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
	<script type="text/javascript" src="{{ config('app.url') }}/js/revolution/jquery.themepunch.tools.min.js"></script>   
	<script type="text/javascript" src="{{ config('app.url') }}/js/revolution/jquery.themepunch.revolution.min.js"></script>

	<script src="{{ config('app.url') }}/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/enscroll-0.5.2.min.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/jquery.scrolly.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/jquery.isotope.min.js"></script>
	<script src="{{ config('app.url') }}/js/isotope-initialize.js"></script>
	<script src="{{ config('app.url') }}/js/script.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function(){
			jQuery('.tp-banner').show().revolution({
				delay:15000,
				startwidth:1170,
				startheight:540,
				autoHeight:"off",
				navigationType:"none",
				hideThumbs:10,
				fullWidth:"on",
				fullScreen:"on",
				fullScreenOffsetContainer:""
			});	

	});
    </script>
    <script>
        $('.sim-thumb').on('click', function() {
            $('#main-product-image').attr('src', $(this).data('image'));
        })
    </script>
    <script>
    </script>
</body>

<!-- Mirrored from html.webinane.com/picstock/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 06:08:42 GMT -->
</html>