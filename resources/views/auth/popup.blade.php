<div class="registration-popup">
      <div class="popup-overlay">
         <div class="popup-container">
            <div class="form-box signup-popup">
               <span class="close-popup"><i class="fa fa-remove"></i></span>
               <div class="title">
                  <h2>Sign Up</h2>
                  <i>You can select from the key</i>
               </div>
               <form class="registration-form" method="POST" action="{{ route('register') }}">
                  <div class="row">
					  @csrf
                  <div class="col-md-12">
                     <div class="field"><i class="ti-user"></i><input type="text" name="name" placeholder="Name" required/></div>
                  </div>
                  <div class="col-md-12">
                     <div class="field"><i class="ti-envelope"></i><input type="email" name="email" placeholder="Email Address" required/></div>
                  </div>
                  <div class="col-md-12">
                     <div class="field"><i class="ti-key"></i><input type="password" name="password" placeholder="Password" required/></div>
                  </div>
                  <div class="col-md-12">
                     <div class="field"><i class="ti-key"></i><input type="password" name="password_confirmation" placeholder="Confirm Password" required/></div>
                  </div>
                  <div class="col-md-12">
                     <div class="field choice"><input type="checkbox" name="check" /><label>I agree to Shutterstock's Website Terms, Privacy Policy, Licensing<br/> Terms and to receive emails that I can opt out of at any time.</label></div>
                  </div>
                  <div class="col-md-12">
                     <div class="field field-button"><input type="submit" value="Create Account"/></div>
                  </div>
            </div>
            </form>
         </div>
         <!-- Sing Up Form -->
         <div class="form-box login-popup">
            <span class="close-popup"><i class="fa fa-remove"></i></span>
            <div class="title">
               <h2>LOGIN</h2>
               <i>You can select from the key</i>
            </div>
			<form class="registration-form" method="post" action="{{ route('login') }}">
				@csrf
               <div class="row">
                  <div class="col-md-12">
                     <div class="field"><i class="ti-envelope"></i><input type="email" name="email" placeholder="Email Address" /></div>
                  </div>
                  <div class="col-md-12">
                     <div class="field"><i class="ti-key"></i><input type="password" name="password" placeholder="Password" /></div>
                  </div>
                  <div class="col-md-12">
                     <div class="field field-button"><input type="submit" value="Login" /></div>
                  </div>
               </div>
            </form>
         </div>
         <!-- Login Form -->
         <div class="packages-popup">
            <span class="close-popup"><i class="fa fa-remove"></i></span>
            <div class="credits">
               <div class="simple-title">
                  <h2>Pay Per Download With  Credits</h2>
                  <p>Download the images, vectors and video clips you need with credits - buy more, save more. </p>
               </div>
               <div class="buy-credits">
                  <div class="credit">
                     <span>3 Credits</span>
                     <div class="credits-price"><strong>$33 USD</strong>SAVE 8%</div>
                     <a class="buy" href="#" title="">BUY CREDITS</a>
                  </div>
                  <div class="credit">
                     <span>3 Credits</span>
                     <div class="credits-price"><strong>$33 USD</strong>SAVE 8%</div>
                     <a class="buy" href="#" title="">BUY CREDITS</a>
                  </div>
                  <div class="credit">
                     <span>3 Credits</span>
                     <div class="credits-price"><strong>$33 USD</strong>SAVE 8%</div>
                     <a class="buy" href="#" title="">BUY CREDITS</a>
                  </div>
                  <div class="credit">
                     <span>3 Credits</span>
                     <div class="credits-price"><strong>$33 USD</strong>SAVE 8%</div>
                     <a class="buy" href="#" title="">BUY CREDITS</a>
                  </div>
                  <div class="credit">
                     <span>3 Credits</span>
                     <div class="credits-price"><strong>$33 USD</strong>SAVE 8%</div>
                     <a class="buy" href="#" title="">BUY CREDITS</a>
                  </div>
               </div>
            </div>
            <div class="subscribe">
               <div class="simple-title">
                  <h2>Pay Per Download With  Credits</h2>
                  <p>Download the images, vectors and video clips you need with credits - buy more, save more. </p>
               </div>
               <div class="best-packages">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="package">
                           <i class="ti-stats-up"></i>
                           <h4>Weekly Packages</h4>
                           <p>Just starting out? Our Pay-As-You-Go plans offer images </p>
                           <strong>$2.64<a href="#" title="">Choose plan</a></strong>
                        </div>
                        <!-- Package -->
                     </div>
                     <div class="col-md-6">
                        <div class="package">
                           <i class="ti-microsoft"></i>
                           <h4>Weekly Packages</h4>
                           <p>Just starting out? Our Pay-As-You-Go plans offer images </p>
                           <strong>$2.64<a href="#" title="">Choose plan</a></strong>
                        </div>
                        <!-- Package -->
                     </div>
                     <div class="col-md-6">
                        <div class="package">
                           <i class="ti-comments-smiley"></i>
                           <h4>Weekly Packages</h4>
                           <p>Just starting out? Our Pay-As-You-Go plans offer images </p>
                           <strong>$2.64<a href="#" title="">Choose plan</a></strong>
                        </div>
                        <!-- Package -->
                     </div>
                     <div class="col-md-6">
                        <div class="package">
                           <i class="ti-briefcase"></i>
                           <h4>Weekly Packages</h4>
                           <p>Just starting out? Our Pay-As-You-Go plans offer images </p>
                           <strong>$2.64<a href="#" title="">Choose plan</a></strong>
                        </div>
                        <!-- Package -->
                     </div>
                  </div>
               </div>
               <span><strong>Need larger credit packs?</strong>  Call +1 876 378 251.</span>
            </div>
         </div>
         <!-- Packages Popup -->
      </div>
   </div>
   </div><!-- Registration Popup -->