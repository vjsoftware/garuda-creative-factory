@if (Auth::check())
	@php
		$userId = Auth::user()->id;
		$now = Carbon\Carbon::now();
		$findMemberShip = DB::table('users')->where('id', $userId)->whereDate('pakage', '>', "$now")->count();
		// dd($findMemberShip);
	@endphp
	@if ($findMemberShip == 0)
		<section>
   <div class="block">
      <div class="container">
         <div class="row">
            <div class="col-md-12 column">
               <div class="title center">
                  <h2>Choose Your Best Package </h2>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
               </div>
               <div class="best-packages">
                  <div class="row">
                     <div class="col-md-4">
                        <div class="package">
                           <i class="ti-stats-up"></i>
                           <h4>Weekly Packages</h4>
                           <p>Just starting out? Our Pay-As-You-Go plans offer images </p>
                           <strong>Rs. 399/-<a href="https://www.payumoney.com/paybypayumoney/#/1D87934233BE5198049A85C5DECC80DD" title="">Choose plan</a></strong>
                        </div>
                        <!-- Package -->
                     </div>
                     <div class="col-md-4">
                        <div class="package">
                           <i class="ti-microsoft"></i>
                           <h4>Monthly Packages</h4>
                           <p>Just starting out? Our Pay-As-You-Go plans offer images </p>
                           <strong>Rs. 1,599/-<a href="https://www.payumoney.com/paybypayumoney/#/05FA3159A46FE6C057DF77253B561730" title="">Choose plan</a></strong>
                        </div>
                        <!-- Package -->
                     </div>
                     <div class="col-md-4">
                        <div class="package">
                           <i class="ti-comments-smiley"></i>
                           <h4>Yearly Packages</h4>
                           <p>Just starting out? Our Pay-As-You-Go plans offer images </p>
                           <strong>Rs. 11,999/-<a href="https://www.payumoney.com/paybypayumoney/#/D7B42FF41657C54AE02DDC2ACEF05220" title="">Choose plan</a></strong>
                        </div>
                        <!-- Package -->
                     </div>
                  </div>
               </div>
               <!-- Best Packages -->
            </div>
         </div>
      </div>
   </div>
</section>
	@else
	<section>
		<br>
		<br>
		<br>
	</section>
	@endif
@else
	<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-12 column">
				<div class="title center">
					<h2>Choose Your Best Package </h2>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
				</div>
				<div class="best-packages">
					<div class="row">
						<div class="col-md-4">
							<div class="package">
							<i class="ti-stats-up"></i>
							<h4>Weekly Packages</h4>
							<p>Just starting out? Our Pay-As-You-Go plans offer images </p>
							<strong>Rs. 399/-<a href="https://www.payumoney.com/paybypayumoney/#/1D87934233BE5198049A85C5DECC80DD" title="">Choose plan</a></strong>
							</div>
							<!-- Package -->
						</div>
						<div class="col-md-4">
							<div class="package">
							<i class="ti-microsoft"></i>
							<h4>Monthly Packages</h4>
							<p>Just starting out? Our Pay-As-You-Go plans offer images </p>
							<strong>Rs. 1,599/-<a href="https://www.payumoney.com/paybypayumoney/#/05FA3159A46FE6C057DF77253B561730" title="">Choose plan</a></strong>
							</div>
							<!-- Package -->
						</div>
						<div class="col-md-4">
							<div class="package">
							<i class="ti-comments-smiley"></i>
							<h4>Yearly Packages</h4>
							<p>Just starting out? Our Pay-As-You-Go plans offer images </p>
							<strong>Rs. 11,999/-<a href="https://www.payumoney.com/paybypayumoney/#/D7B42FF41657C54AE02DDC2ACEF05220" title="">Choose plan</a></strong>
							</div>
							<!-- Package -->
						</div>
					</div>
				</div>
				<!-- Best Packages -->
				</div>
			</div>
		</div>
	</div>
	</section>
@endif