<!DOCTYPE html>

<!-- Mirrored from html.webinane.com/picstock/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 06:08:22 GMT -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Garuda Creative Factory</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{ config('app.url') }}/css/icons.css">
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/style.css" />
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/responsive.css" />
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/color.css" />

	<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/extralayers.css" media="screen" />	
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/settings.css" media="screen" />

    <style>
        ul li {
            list-style: none;
            float: left;
        }
.product-image-gallery {
  background-color: #fefefe;
  /* padding: 0.5rem; */
}

.pdp-product-image {
  margin-bottom: 20px;
}

.product-thumbs a {
  margin-left: 8px;
  margin-right: 8px;
  padding: 0 !important;
}

.product-thumbs a img {
  display: inline-block;
  max-width: 100%;
  margin-bottom: 1rem;
  border: solid 4px #fefefe;
  border-radius: 0;
  box-shadow: 0 0 0 1px rgba(10, 10, 10, 0.2);
  line-height: 0;
  border-radius: 3px;
  width: 50px;
  height: 50px;
  border: none;
}


    </style>
</head>
<body>
<div class="theme-layout">
	@include('layouts/navbar')	

	
<section>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-12 column">
						<div class="detail-page">
							<div class="single-img">
                                <div class="row align-center">
                                <div class="product-image-gallery">
                                    <img class="pdp-product-image" id="main-product-image" src="{{ config('app.url') }}/images/products/pencil-sketch/1.jpg" height="300px" alt="">
                                    <br>
                                    <ul class="menu product-thumbs align-center">
                                    <li> <a class="sim-thumb" data-image="{{ config('app.url') }}/images/products/pencil-sketch/1.jpg"><img src="{{ config('app.url') }}/images/products/pencil-sketch/1.jpg" style="height: 100px; width: auto;" alt=""></a> </li>
                                    <li> <a class="sim-thumb" data-image="{{ config('app.url') }}/images/products/pencil-sketch/2.jpg"><img src="{{ config('app.url') }}/images/products/pencil-sketch/2.jpg" style="height: 100px; width: auto;" alt=""></a> </li>
                                    <li> <a class="sim-thumb" data-image="{{ config('app.url') }}/images/products/pencil-sketch/3.jpg"><img src="{{ config('app.url') }}/images/products/pencil-sketch/3.jpg" style="height: 100px; width: auto;" alt=""></a> </li>
                                    
                                    </ul>
                                </div>
                                </div>
							</div>
							<form action="{{ route('sketchStore') }}" method="POST" enctype="multipart/form-data">
							<div class="single-img-detail">
                                <h2>Pencil Sketch:</h2>
                                <hr>
                                <span>Options</span>
                                <br>
                                <br>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Number of Faces / People:</label>
                                    <div class="col-sm-7">
										@csrf
                                    <select name="faces" id="faces" class="form-control" required>
                                            <option value="0" selected> 1 Face </option>
                                            <option value="200"> 2 Face </option>
                                            <option value="400"> 3 Face </option>
                                            <option value="600"> 4 Face </option>
                                            <option value="800"> 5 Face </option>
                                            <option value="1000"> 6 Face </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Format / Size:</label>
                                    <div class="col-sm-7">
                                    <select name="paper" id="paper" class="form-control" required>
										<option value="2000">A0 Print - 32 x 48 inches</option>
										<option value="4500">A1 Print &amp; Frame - 24 x 32 inches</option>
										<option value="2400">A2 Print &amp; Frame - 16 x 24 inches</option>
										<option value="1300">A3 Print &amp; Frame - 12 x 16 inches</option>
										<option value="800" selected>A4 Print &amp; Frame - 8 x 12 inches</option>
										<option value="500">A5 Print &amp; Frame - 6 x 8 inches</option>
										<option value="400">A6 Print &amp; Frame - 4 x 6 inches</option>
										<option value="0">HD Softcopy(Only Email, No Print, No Frame)</option>
									</select>
										
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Time Duration:</label>
                                    <div class="col-sm-7">
                                    <select name="duration" id="duration" class="form-control" required>
                                            <option value="0" selected> Normal </option>
                                            <option value="200"> Express </option>
                                        </select>
                                    </div>
								</div>
								<br>
                                <div class="form-group row">
                                    <h2 for="staticEmail" class="col-sm-5 col-form-label">Price:</h2>
                                    <h2 class="col-sm-7" id="price">1290</h2>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Image:</label>
                                    <div class="col-sm-7">
                                        <input type="file" name="image" id="" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">Mobile Number <span class="text-warning">*</span>:</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="mobile" id="" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-5 col-form-label">E@Mail <span class="text-warning">*</span>:</label>
                                    <div class="col-sm-7">
                                        <input type="email" name="email" id="" class="form-control" required>
                                    </div>
                                </div>

                                <h1>Contact For Update</h1>
                                <a data-letters="Whats App" class="theme-btn" style="background: #2159d8" href="https://api.whatsapp.com/send?phone=919966775234&text=I%27m%20interested%20in%20your%20updates#" target="_blank" title="Whats App">Whats App</a>
								<br><br>
								{{-- <input type="submit"  name="Order Now" id="" value> --}}
								<button class="theme-btn" type="submit">Order Now</button>
								{{-- <a data-letters="Order Now" class="theme-btn" href="#" title="">Order Now</a> --}}
							</div>
						</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


	@include('layouts/footer')

	@include('auth/popup')

	<div class="wishlist-btn"><a href="{{ config('app.url') }}/wishlist.html" title=""><i class="fa fa-heart"></i></a></div>
	

	<script src="{{ config('app.url') }}/js/jquery.min.js" type="text/javascript"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
	<script type="text/javascript" src="{{ config('app.url') }}/js/revolution/jquery.themepunch.tools.min.js"></script>   
	<script type="text/javascript" src="{{ config('app.url') }}/js/revolution/jquery.themepunch.revolution.min.js"></script>

	<script src="{{ config('app.url') }}/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/enscroll-0.5.2.min.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/jquery.scrolly.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/jquery.isotope.min.js"></script>
	<script src="{{ config('app.url') }}/js/isotope-initialize.js"></script>
	<script src="{{ config('app.url') }}/js/script.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function(){
			jQuery('.tp-banner').show().revolution({
				delay:15000,
				startwidth:1170,
				startheight:540,
				autoHeight:"off",
				navigationType:"none",
				hideThumbs:10,
				fullWidth:"on",
				fullScreen:"on",
				fullScreenOffsetContainer:""
			});	

	});
    </script>
    <script>
        $('.sim-thumb').on('click', function() {
            $('#main-product-image').attr('src', $(this).data('image'));
        })
    </script>
    <script>
        $('select').change(function() {
			var faces = parseInt($('#faces').val());
            var paper = parseInt($('#paper').val());
			var duration = parseInt($('#duration').val());
			var makingPrice = parseInt(490);
			var price = makingPrice + faces + paper + duration;
            // var one = 990;
            // var two = 1290;
            // var three = 1490;
            // var four = 1690;
            // var five = 1890;
            // var six = 2090;
			// var prc = faces * 200;
            // var price = one + prc - 100;
			// if(faces == 1) {
			// 	price = 990;
			// }
            $('#price').html(price);

        })
    </script>

</body>

<!-- Mirrored from html.webinane.com/picstock/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 06:08:42 GMT -->
</html>