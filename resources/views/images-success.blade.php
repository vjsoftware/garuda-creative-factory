@php
    use App\Image;
@endphp
<!DOCTYPE html>

<!-- Mirrored from html.webinane.com/picstock/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Nov 2019 06:08:22 GMT -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Garuda Creative Factory</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{ config('app.url') }}/css/icons.css">
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/style.css" />
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/responsive.css" />
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/color.css" />

	<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/extralayers.css" media="screen" />	
	<link rel="stylesheet" type="text/css" href="{{ config('app.url') }}/css/settings.css" media="screen" />

</head>
<body>
<div class="theme-layout">
	@include('layouts/navbar')	
    <section>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-12 column">
						<div class="page-top-search">
							<div class="main-search fadeInUp">
								<form>
									<input type="text" placeholder="Search Here Your Keywords">
									<button><i class="fa fa-search"></i></button>
								</form>
							</div><!-- Main Search -->
						</div><!-- Page Top Search Bar -->
					</div>
				</div>
			</div>
		</div>
	</section>

    @php
        $slug = session('buyId');
        $image = Image::find($slug);
    @endphp
	
<section>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-12 column">
						<div class="detail-page">
                            @php
                                $status=$postData["status"];
                                $firstname=$postData["firstname"];
                                $amount=$postData["amount"];
                                $txnid=$postData["txnid"];
                                $posted_hash=$postData["hash"];
                                $key=$postData["key"];
                                $productinfo=$postData["productinfo"];
                                $email=$postData["email"];
                                $salt= "nIAHEU5BFW";

                                // Salt should be same Post Request
                            @endphp
                            @if (isset($postData["additionalCharges"]))
                                @php
                                    $additionalCharges=$postData["additionalCharges"];
                                    $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
                                @endphp
                            @else
                                @php
                                    $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
                                    $hash = hash("sha512", $retHashSeq);
                                @endphp
                                @if ($hash != $posted_hash)
                                    <h4>{{ "Invalid Transaction. Please try again" }}</h4>
                                @else
                                <div class="single-img"><img src="{{ Voyager::image($image['image']) }}" alt=""></div>
                                <div class="single-img-detail">
                                    <h2>{{ $image['name'] }}:</h2>
                                    <span>{{ $image['desc'] }}.</span>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <hr>
                                    <h5>{{ "Thank You. Your order status is success." }}.</h5>
                                    <h5>Your Transaction ID for this transaction is <span class="text-warning">{{ $txnid }}</span>.</h5>
                                    <h5>We have received a payment of <span class="text-warning">Rs.{{ $amount }}</span>.</h5>
                                    <a data-letters="Download" class="theme-btn" download  href="{{ Voyager::image($image['image']) }}">Download</a>
                                </div>
                                @endif
                            @endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


	@include('layouts/footer')


	@include('auth/popup')

	<div class="wishlist-btn"><a href="{{ config('app.url') }}/wishlist.html" title=""><i class="fa fa-heart"></i></a></div>
	

	<script src="{{ config('app.url') }}/js/jquery.min.js" type="text/javascript"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
	<script type="text/javascript" src="{{ config('app.url') }}/js/revolution/jquery.themepunch.tools.min.js"></script>   
	<script type="text/javascript" src="{{ config('app.url') }}/js/revolution/jquery.themepunch.revolution.min.js"></script>

	<script src="{{ config('app.url') }}/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/enscroll-0.5.2.min.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/jquery.scrolly.js" type="text/javascript"></script>
	<script src="{{ config('app.url') }}/js/jquery.isotope.min.js"></script>
	<script src="{{ config('app.url') }}/js/isotope-initialize.js"></script>
	<script src="{{ config('app.url') }}/js/script.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function(){
			jQuery('.tp-banner').show().revolution({
				delay:15000,
				startwidth:1170,
				startheight:540,
				autoHeight:"off",
				navigationType:"none",
				hideThumbs:10,
				fullWidth:"on",
				fullScreen:"on",
				fullScreenOffsetContainer:""
			});	

	});
	</script>
	<script>
		function ajaxbuyimagebutton(id) {
			// console.log(id);
			var data = {buyId: id}
			$.ajax({
			type:'POST',
			url:'/ajaxbuyimage',
			data: data,
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success:function(data) {
				if (data = 'sucess') {
					window.location.href = "https://www.payumoney.com/paybypayumoney/#/91CCD06464D6B5E7D3B496D9018137CB";
				}
				// alert(data);
				// $("#msg").html(data.msg);
			}
		});
		}
</script>
</body>

</html>