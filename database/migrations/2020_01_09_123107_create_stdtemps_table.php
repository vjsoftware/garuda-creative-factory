<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStdtempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stdtemps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('bname');
            $table->string('gname');
            $table->string('date');
            $table->string('time');
            $table->string('venue');
            $table->string('image');
            $table->string('image2');
            $table->string('email');
            $table->string('video');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stdtemps');
    }
}
