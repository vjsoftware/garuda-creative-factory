<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('banner');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'layout' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:82048',
            'attachment' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:82048',
            ]);
            
            if ($request->hasFile('layout')) {
                $image = $request->file('layout');
                $path = Date('F') . Date('Y');
                $time = time();
                $destinationPath = public_path('storage/banner/' . $path);
                $name = '/banner/' . $path . '/' . $time . '.' . $image->getClientOriginalExtension();
                $nameToDb = 'banner/' . $path . '/' . $time . '.' . $image->getClientOriginalExtension();
                $saveImagePath = 'banner/' . $path . '/' . $time . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $name);
                // return $saveImagePath;

            // Attachment's
            $saveImagePathAtt = '';
            if ($request->hasFile('attachment')) {
                $image = $request->file('attachment');
                $path = Date('F') . Date('Y');
                $time = time();
                $destinationPath = storage_path('app/public/banner/' . $path);
                $name = '/banner/' . $path . '/' . $time . '.' . $image->getClientOriginalExtension();
                $nameToDb = 'banner/' . $path . '/' . $time . '.' . $image->getClientOriginalExtension();
                $saveImagePathAtt = 'banner/' . $path . '/' . $time . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $name);
            }
            

            $newPaining = new Banner();
            $newPaining->width = $request['width'];
            $newPaining->height = $request['height'];
            $newPaining->size = $request['size'];
            $newPaining->type = $request['type'];
            $newPaining->email = $request['email'];
            $newPaining->notes = $request['notes'];
            $newPaining->layout = $saveImagePath;
            $newPaining->attachment = $saveImagePathAtt;
            $newPaining->save();
            // $this->save();
            return "upload Success";
            return back()->with('success', 'Image Upload successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        //
    }
}
