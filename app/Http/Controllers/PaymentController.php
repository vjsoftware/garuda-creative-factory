<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Return_;

class PaymentController extends Controller
{

    // Buy Image
    public function image(Request $request)
    {
        // return $request['buyId'];
        session(['buyId' => $request['buyId']]);
        // return session('buyId');
        if(session('buyId') != '') {
            return 'sucess';
        } else {
            return 'no';
        }
    }

    // Buy png
    public function png(Request $request)
    {
        // return $request['buyId'];
        session(['buyPngId' => $request['buyId']]);
        // return session('buyId');
        if(session('buyPngId') != '') {
            return 'sucess';
        } else {
            return 'no';
        }
    }

    public function test() {
        return View('payu');
    }
}
