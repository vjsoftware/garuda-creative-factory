<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tzsk\Payu\Facade\Payment;

use App\Digitalpaintingtemp;
use App\Digitalpainting;

class PayumoneyController extends Controller
{
    public function paymentendpoint($id)
    {

        $productInfo = Digitalpaintingtemp::find($id);
        $attributes = [
            'txnid' => strtoupper(str_random(8)), # Transaction ID.
            'amount' => $productInfo['price'], # Amount to be charged.
            'productinfo' => "Digital Painting",
            'firstname' => $productInfo['name'], # Payee Name.
            'email' => $productInfo['email'], # Payee Email Address.
            'phone' => $productInfo['mobile'], # Payee Phone Number.
            'udf1' => $productInfo['id'], # Digital Painting Temp ID.
        ];

        return Payment::make($attributes, function ($then) {
            $then->redirectTo('payment/status');
            // return $payment = Payment::capture();
        });
    }

    public function status(Request $request)
    {
        $payment = Payment::capture();
        $data = $payment->getData();
        // return json_encode($data);
        if($data->status == 'success') {
            // return $data->udf1;
            $productInfo = Digitalpaintingtemp::find($data->udf1);
            $digitalPaint = new Digitalpainting();
            $digitalPaint->name = $productInfo['name'];
            $digitalPaint->mobile = $productInfo['mobile'];
            $digitalPaint->email = $productInfo['email'];
            $digitalPaint->faces = $productInfo['faces'];
            $digitalPaint->paper = $productInfo['paper'];
            $digitalPaint->duration = $productInfo['duration'];
            $digitalPaint->image = $productInfo['image'];
            $digitalPaint->price = $productInfo['price'];
            $digitalPaint->tnxid = $data->txnid;
            $digitalPaint->save();
            return view('payusuccess')->with('data', $data);
        } else {
            return view('payufail')->with('data', $data);
        }
        // return json_encode($data);
        // return dd(json_decode($payment['data']));

    }
}
