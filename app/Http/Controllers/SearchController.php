<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Import Query Builder
use Spatie\QueryBuilder\QueryBuilder;

// Import Model's
use App\Image;
use App\Png;


class SearchController extends Controller
{
    public function search(Request $request)
    {
        // return $request;
        $image = $request['search'];
        $type = $request['type'];
        // return $type;
        if($type == 1) {            
            return redirect("/filter?filter[name]=$image");
        } else {
            return redirect("/filterpng?filter[name]=$image");
        }
        
    }

    // Image's
    public function filter()
    {
        $images = QueryBuilder::for(Image::class)
        ->allowedFilters(['name'])
        ->get();
        // return $images;
        return view('searchresult')->with('data', $images);
    }

    // Png's
    public function filterpng()
    {
        $images = QueryBuilder::for(Png::class)
        ->allowedFilters(['name'])
        ->get();
        // return $images;
        return view('searchresultpng')->with('data', $images);
    }
}
