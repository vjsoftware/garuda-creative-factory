<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Std;
use App\Stdtemps;

class StdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('stdviews');
    }

    public function success(Request $request)
    {
        $tempId = session('stdBuyId');
        // return $tempId;
        $std = new Std();
        $tasks = Stdtemps::find($tempId);
        $std->fname = $tasks['fname'];
        $std->bname = $tasks['bname'];
        $std->gname = $tasks['gname'];
        $std->date = $tasks['date'];
        $std->time = $tasks['time'];
        $std->venue = $tasks['venue'];
        $std->image = $tasks['image'];
        $std->image2 = $tasks['image2'];
        $std->email = $tasks['email'];
        $std->video = $tasks['video'];
        $std->save();
        // return $std;
        session(['stdBuyId' => '']);
        return view('std-success')->with('postData', $request);
        // return $slug;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return 'lllll';
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:82048',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $path = Date('F') . Date('Y');
            $time = time();
            $destinationPath = public_path('storage/std/' . $path);
            $name = '/std/' . $path . '/' . $time . '.' . $image->getClientOriginalExtension();
            $nameToDb = 'std/' . $path . '/' . $time . '.' . $image->getClientOriginalExtension();
            // $saveImagePath = [{"download_link":"caricatures\\December2019\\Hmnl8PvcMovMoIalMCnD.jpg","original_name":"4.jpg"}]
            // $saveImagePath = "[{". '"download_link":"'.$nameToDb.'","original_name":"'. $time .'.'.$image->getClientOriginalExtension() . '"}]';
            $saveImagePath = 'std/' . $path . '/' . $time . '.' . $image->getClientOriginalExtension();
            // return $saveImagePath;
            $image->move($destinationPath, $name);

            // Save Second Image
            $saveImagePath2 = '';
            if ($request->hasFile('image2')) {
                $image = $request->file('image2');
                $path = Date('F') . Date('Y');
                $time = time() + 1;
                $destinationPath = public_path('storage/std/' . $path);
                $name = '/std/' . $path . '/' . $time . '.' . $image->getClientOriginalExtension();
                $nameToDb = 'std/' . $path . '/' . $time . '.' . $image->getClientOriginalExtension();
                // $saveImagePath = [{"download_link":"caricatures\\December2019\\Hmnl8PvcMovMoIalMCnD.jpg","original_name":"4.jpg"}]
                // $saveImagePath = "[{". '"download_link":"'.$nameToDb.'","original_name":"'. $time .'.'.$image->getClientOriginalExtension() . '"}]';
                $saveImagePath2 = 'std/' . $path . '/' . $time . '.' . $image->getClientOriginalExtension();
                // return $saveImagePath;
                $image->move($destinationPath, $name);
            }
            
            $video = "https://www.youtube.com/watch?v=".$request['video'];

            $newPaining = new Stdtemps();
            $newPaining->fname = $request['fname'];
            $newPaining->bname = $request['bname'];
            $newPaining->gname = $request['gname'];
            $newPaining->image = $saveImagePath;
            $newPaining->image2 = $saveImagePath2;
            $newPaining->date = $request['date'];
            $newPaining->time = $request['time'];
            $newPaining->venue = $request['venue'];
            $newPaining->email = $request['email'];
            $newPaining->video = $video;
            $newPaining->save();
            // $this->save();
            // return $newPaining['id'];
            session(['stdBuyId' => $newPaining['id']]);
            // return redirect()->route('stdsuccess');
            return redirect('https://www.payumoney.com/paybypayumoney/#/3931327469E23433ADD6DAB10E369136');
            return back()->with('success', 'Image Upload successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('std')->with('id', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
