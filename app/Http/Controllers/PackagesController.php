<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon;

use Auth;

use DB;

use App\User;

class PackagesController extends Controller
{

    // Monthly Package
    public function weekly(Request $request)
    {
        // // return Carbon\Carbon::now();
        $userId = Auth::user()->id;
        // // return $userId;
        // $now = Carbon\Carbon::now();
        // // return $now;
        // DB::enableQueryLog();
        // $findMemberShip = DB::table('users')->where('id', $userId)->whereDate('pakage', '>', "$now")->count();
        // // dd(DB::getQueryLog());
        // return $findMemberShip;
        
        $user = User::find($userId);
        $user->pakage = Carbon\Carbon::now()->addDay(7);
        $user->save();
        return view('weekly-success')->with('postData', $request);
    }

    // Monthly Package
    public function monthly(Request $request)
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);
        $user->pakage = Carbon\Carbon::now()->addDay(31);
        $user->save();
        return view('monthly-success')->with('postData', $request);
    }

    // Yearly Package
    public function yearly(Request $request)
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);
        $user->pakage = Carbon\Carbon::now()->addDay(365);
        $user->save();
        return view('yearly-success')->with('postData', $request);
    }
}
