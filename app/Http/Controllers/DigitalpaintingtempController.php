<?php

namespace App\Http\Controllers;

use App\Digitalpaintingtemp;
use Illuminate\Http\Request;

class DigitalpaintingtempController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Digitalpaintingtemp  $digitalpaintingtemp
     * @return \Illuminate\Http\Response
     */
    public function show(Digitalpaintingtemp $digitalpaintingtemp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Digitalpaintingtemp  $digitalpaintingtemp
     * @return \Illuminate\Http\Response
     */
    public function edit(Digitalpaintingtemp $digitalpaintingtemp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Digitalpaintingtemp  $digitalpaintingtemp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Digitalpaintingtemp $digitalpaintingtemp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Digitalpaintingtemp  $digitalpaintingtemp
     * @return \Illuminate\Http\Response
     */
    public function destroy(Digitalpaintingtemp $digitalpaintingtemp)
    {
        //
    }
}
