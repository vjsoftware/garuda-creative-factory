<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Digitalpainting;
use App\Digitalpaintingtemp;

class DigitalPaintingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('digital-painting');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:82048',
            ]);
            
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $path = Date('F').Date('Y');
                $time = time();
                $destinationPath = public_path('storage/digitalpaintings/'.$path);
                // return public_path();
                $name = '/digitalpaintings/'.$path.'/'. $time . '.' . $image->getClientOriginalExtension();
                $nameToDb = 'digitalpaintings/'.$path.'/'. $time . '.' . $image->getClientOriginalExtension();
                // $saveImagePath = [{"download_link":"digitalpaintings\\December2019\\Hmnl8PvcMovMoIalMCnD.jpg","original_name":"4.jpg"}]
                // $saveImagePath = "[{". '"download_link":"'.$nameToDb.'","original_name":"'. $time .'.'.$image->getClientOriginalExtension() . '"}]';
                $saveImagePath = 'digitalpaintings/' . $path . '/' . $time . '.' . $image->getClientOriginalExtension();
                    // return $saveImagePath;
                $image->move($destinationPath, $name);
                $faces = 1;
                if($request['faces'] != 0) {
                    $faces = ($request['faces'] / 100) * 1;
                }
                // return $faces;
                $paper = 'HD Image';
                $paperSize = $request['paper'];
                if ($paperSize == '2000') {
                    $paper = 'A0';
                } elseif ($paperSize == '4500') {
                    $paper = 'A1';
                } elseif ($paperSize == '2400') {
                    $paper = 'A2';
                } elseif ($paperSize == '1300') {
                    $paper = 'A3';
                } elseif ($paperSize == '800') {
                    $paper = 'A4';
                } elseif ($paperSize == '500') {
                    $paper = 'A5';
                } elseif ($paperSize == '400') {
                    $paper = 'A6';
                }

                $durationCalc = $request['duration'];
                $duration = "Normal";
                if($durationCalc != 0) {
                    $duration = "Express";
                }
                $newPaining = new Digitalpaintingtemp();
                $newPaining->faces = $faces;
                $newPaining->paper = $paper;
                $newPaining->duration = $duration;
                $newPaining->image = $saveImagePath;
                $newPaining->name = $request['name'];
                $newPaining->mobile = $request['mobile'];
                $newPaining->email = $request['email'];
                $newPaining->price = $request['price'];
                $newPaining->save();
                return redirect()->route('payumoney', $newPaining['id']);
                // $this->save();

                
                // return "upload Success";
                // return back()->with('success', 'Image Upload successfully');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
